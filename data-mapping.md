# Advanced table

## Package column

Display:
  * PackageData['general']['name']
Link:
  * PackageName.get_absolute_url()
Popup:
  * Name: PackageData['general']['name']
  * Short Description: PackageName.short_description
  * Version: PackageData['general']['version']
  * Maintainer:  PackageData['general']['maintainer']['name']
  * Uploarders:  PackageData['general']['uploarders'] -> [{'name', 'email'}]
  * Architectures: PackageData['general']['architectures'] -> List of names
  * Std-version: PackageData['general']['standards_version'] -> **I don't know what it is.**
  * Binaries: PackageName.main_version.binarypackage_set.all()

## Repository column

Display:
  * PackageData['general']['vcs']['type']
Popup:
  * VCS URL: PackageData['general']['vcs']['url']
  * VCS Browser URL (LINK): PackageData['general']['vcs']['browser']
Missing:
  * QA link


## Versions column

Display: 
  * PackageData['versions']['version_list'] do |version|:
    * Repository name -> version['repository']['suite']
Popup:
  * PackageData['versions']['version_list'] do |version|:
    * Repository name -> version['repository']['suite']
    * Repository name -> version['repository']['codename']
    * Repository name -> version['version']

## Bugs

Display:
  * PackageBugStats.each do |stat|
    * Category stat['category_name']
    * Count stat['bug_count']
  * All bugs - sum all bugs listed above

## Archive - TODO
## Upstream - TODO
## Lintian Check - TODO


